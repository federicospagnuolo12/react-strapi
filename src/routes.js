import React, {Component} from 'react'
import {Route, Switch} from 'react-router-dom'

//componentes
import App from './App'
import AdminSite from './components/adminOnly/'
import Page404 from './components/Page404'
import UserSite from "./components/userSite/"

class AppRoutes extends Component{
    render(){
        return(
        <Switch>
            <Route exact path='/' component={App} />
            <Route exact path='/userSite' component={UserSite} />
            <Route path='/adminOnly' component={AdminSite} />
            <Route component={Page404} />
        </Switch>
        );
    }

}
export default AppRoutes;