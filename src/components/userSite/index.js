import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import Header from '../Hub/Header'
class UserSite extends Component {
        componentDidMount(){
            const carrera = localStorage.getItem('carrera');
            const token = localStorage.getItem('token');
            const role = localStorage.getItem('role');
            if (role != '"Alumno"'){
                window.location.replace('/');
            }else{
                fetch('http://localhost:1337/carreras/'+carrera, {
                        method: 'get',
                        body: null,
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                })
                .then(response => response.json())
                .then(data => {
                    localStorage.setItem('Career',JSON.stringify(data.Nombre));
                });
            }
        }

    render() {
        const User = localStorage.getItem('userName');
        const carrera = localStorage.getItem('Career');
        return (
            <div>
                <Header />
                <Card style={{width:'250px', textAlign:'center'}}>
                    <Card.Body>
                        <Card.Title>{User}</Card.Title>
                        <Card.Text>
                            {carrera}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}
export default UserSite; 