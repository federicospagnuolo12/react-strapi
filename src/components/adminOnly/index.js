import React, { Component } from 'react'
import AbmAlumnos from './abmAlumnos'
import AbmCarreras from './abmCarreras'
import Accordion from 'react-bootstrap/Accordion'
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'
import Header from '../Hub/Header'
class AdminSite extends Component {
    constructor(props){
      super(props);

      this.state ={
        alumnos:[],
        carreras:[],
      }
    }
    componentDidMount(){
        const token = localStorage.getItem('token');
        const role = localStorage.getItem('role');
        if (role !== '"Admin"' ) {window.location.replace('/');}
        else{
            fetch('http://localhost:1337/carreras', {
              method: 'get',
              body: null,
              headers: {
                'Authorization': `Bearer ${token}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            })
            .then(response => response.json())
            .then(data => {
              this.setState({
                carreras: data
              })
            });
            fetch('http://localhost:1337/alumnos', {
            method: 'get',
            body: null,
            headers: {
              'Authorization': `Bearer ${token}`,
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
          })
          .then(response => response.json())
          .then(data => {
            this.setState({
              alumnos: data
            });
          });
          }
    }
    
    
    render() {
        return (
            <div>
                <Header />
                <Accordion defaultActiveKey="0" style={{width:'450px',textAlign:'center'}}>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="0">
                        Carreras
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body>
                        {
                          this.state.carreras.map((c,i)=>
                            <h4 style={{border:'solid black 1px'}}>{c.Nombre}</h4>
                          )
                        }
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="1">
                        Alumnos
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="1">
                      <Card.Body>{
                          this.state.alumnos.map((c,i)=>
                            <h4 style={{border:'solid black 1px'}}>{c.Nombre}</h4>
                          )
                        }</Card.Body>
                    </Accordion.Collapse>
                </Card>
                </Accordion>
               <AbmCarreras />
               <AbmAlumnos />
            </div>
        )
    }
}
export default AdminSite;