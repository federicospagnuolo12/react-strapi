import React, { Component } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

class Header extends Component {
    logout(){
        localStorage.clear();
        window.location.replace('/');
    }
    render() {
        return (
            <div>
                <Navbar bg="light" variant="light">
                    <Navbar.Brand href="#">Navbar</Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link href="#">Mi</Nav.Link>
                    <Nav.Link href="#">Strapi-React</Nav.Link>
                    <Nav.Link href="#">App</Nav.Link>
                    </Nav>
                    <Form inline>
                    <Button variant="secondary" onClick={this.logout}>
                        logout
                    </Button>           
                    </Form>
                </Navbar>
            </div>
        )
    }
}
export default Header;